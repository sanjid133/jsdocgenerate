/**
 * Created by sanjid on 2/6/15.
 */
/** @namespace */
var util = {
    /**
     *
     * @param times
     * @returns {string}
     * @param st
     */
    repeat: function(st, times) {
        if (times === undefined || times < 1) {
            times = 1;
        }
        return new Array(times+1).join(st);
    }
};